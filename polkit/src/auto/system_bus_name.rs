// Generated by gir (https://github.com/gtk-rs/gir @ 0e476ab5c1de)
// from /usr/share/gir-1.0 (@ ???)
// DO NOT EDIT

use crate::{Subject, UnixUser};
use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::boxed::Box as Box_;

glib::wrapper! {
    /// An object that represents a process owning a unique name on the system bus.
    ///
    /// ## Properties
    ///
    ///
    /// #### `name`
    ///  The unique name on the system message bus.
    ///
    /// Readable | Writeable | Construct
    ///
    /// # Implements
    ///
    /// [`trait@glib::ObjectExt`], [`SubjectExt`][trait@crate::prelude::SubjectExt]
    #[doc(alias = "PolkitSystemBusName")]
    pub struct SystemBusName(Object<ffi::PolkitSystemBusName, ffi::PolkitSystemBusNameClass>) @implements Subject;

    match fn {
        type_ => || ffi::polkit_system_bus_name_get_type(),
    }
}

impl SystemBusName {
    /// Gets the unique system bus name for `self`.
    ///
    /// # Returns
    ///
    /// The unique system bus name for `self`. Do not
    /// free, this string is owned by `self`.
    #[doc(alias = "polkit_system_bus_name_get_name")]
    #[doc(alias = "get_name")]
    pub fn name(&self) -> glib::GString {
        unsafe { from_glib_none(ffi::polkit_system_bus_name_get_name(self.to_glib_none().0)) }
    }

    /// Synchronously gets a [`UnixProcess`][crate::UnixProcess] object for `self`
    /// - the calling thread is blocked until a reply is received.
    /// ## `cancellable`
    /// A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
    ///
    /// # Returns
    ///
    /// A [`UnixProcess`][crate::UnixProcess] object or [`None`] if `error` is set.
    #[doc(alias = "polkit_system_bus_name_get_process_sync")]
    #[doc(alias = "get_process_sync")]
    pub fn process_sync(
        &self,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<Option<Subject>, glib::Error> {
        unsafe {
            let mut error = std::ptr::null_mut();
            let ret = ffi::polkit_system_bus_name_get_process_sync(
                self.to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    /// Synchronously gets a [`UnixUser`][crate::UnixUser] object for `self`;
    /// the calling thread is blocked until a reply is received.
    /// ## `cancellable`
    /// A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
    ///
    /// # Returns
    ///
    /// A [`UnixUser`][crate::UnixUser] object or [`None`] if `error` is set.
    #[doc(alias = "polkit_system_bus_name_get_user_sync")]
    #[doc(alias = "get_user_sync")]
    pub fn user_sync(
        &self,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<Option<UnixUser>, glib::Error> {
        unsafe {
            let mut error = std::ptr::null_mut();
            let ret = ffi::polkit_system_bus_name_get_user_sync(
                self.to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    /// Sets the unique system bus name for `self`.
    /// ## `name`
    /// A unique system bus name.
    #[doc(alias = "polkit_system_bus_name_set_name")]
    pub fn set_name(&self, name: &str) {
        unsafe {
            ffi::polkit_system_bus_name_set_name(self.to_glib_none().0, name.to_glib_none().0);
        }
    }

    /// Creates a new [`SystemBusName`][crate::SystemBusName] for `name`.
    /// ## `name`
    /// A unique system bus name.
    ///
    /// # Returns
    ///
    /// A [`SystemBusName`][crate::SystemBusName]. Free with `g_object_unref()`.
    #[doc(alias = "polkit_system_bus_name_new")]
    pub fn new(name: &str) -> Subject {
        unsafe { from_glib_full(ffi::polkit_system_bus_name_new(name.to_glib_none().0)) }
    }

    #[doc(alias = "name")]
    pub fn connect_name_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_name_trampoline<F: Fn(&SystemBusName) + 'static>(
            this: *mut ffi::PolkitSystemBusName,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::name\0".as_ptr() as *const _,
                Some(std::mem::transmute::<_, unsafe extern "C" fn()>(
                    notify_name_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl std::fmt::Display for SystemBusName {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(&self.name())
    }
}
