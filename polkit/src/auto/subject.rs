// Generated by gir (https://github.com/gtk-rs/gir @ 0e476ab5c1de)
// from /usr/share/gir-1.0 (@ ???)
// DO NOT EDIT

use glib::{prelude::*, translate::*};
use std::{boxed::Box as Box_, pin::Pin};

glib::wrapper! {
    /// [`Subject`][crate::Subject] is an abstract type for representing one or more
    /// processes.
    ///
    /// # Implements
    ///
    /// [`SubjectExt`][trait@crate::prelude::SubjectExt]
    #[doc(alias = "PolkitSubject")]
    pub struct Subject(Interface<ffi::PolkitSubject, ffi::PolkitSubjectIface>);

    match fn {
        type_ => || ffi::polkit_subject_get_type(),
    }
}

impl Subject {
    pub const NONE: Option<&'static Subject> = None;

    /// Creates an object from `str` that implements the [`Subject`][crate::Subject]
    /// interface.
    /// ## `str`
    /// A string obtained from [`SubjectExt::to_string()`][crate::prelude::SubjectExt::to_string()].
    ///
    /// # Returns
    ///
    /// A [`Subject`][crate::Subject] or [`None`] if `error` is
    /// set. Free with `g_object_unref()`.
    #[doc(alias = "polkit_subject_from_string")]
    pub fn from_string(str: &str) -> Result<Subject, glib::Error> {
        unsafe {
            let mut error = std::ptr::null_mut();
            let ret = ffi::polkit_subject_from_string(str.to_glib_none().0, &mut error);
            if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }
}

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::Subject>> Sealed for T {}
}

/// Trait containing all [`struct@Subject`] methods.
///
/// # Implementors
///
/// [`Subject`][struct@crate::Subject], [`SystemBusName`][struct@crate::SystemBusName], [`UnixProcess`][struct@crate::UnixProcess], [`UnixSession`][struct@crate::UnixSession]
pub trait SubjectExt: IsA<Subject> + sealed::Sealed + 'static {
    #[doc(alias = "polkit_subject_equal")]
    fn equal(&self, b: &impl IsA<Subject>) -> bool {
        unsafe {
            from_glib(ffi::polkit_subject_equal(
                self.as_ref().to_glib_none().0,
                b.as_ref().to_glib_none().0,
            ))
        }
    }

    /// Asynchronously checks if `self` exists.
    ///
    /// When the operation is finished, `callback` will be invoked in the
    /// <link linkend="g-main-context-push-thread-default">thread-default
    /// main loop`</link>` of the thread you are calling this method
    /// from. You can then call `polkit_subject_exists_finish()` to get the
    /// result of the operation.
    /// ## `cancellable`
    /// A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
    /// ## `callback`
    /// A `GAsyncReadyCallback` to call when the request is satisfied
    #[doc(alias = "polkit_subject_exists")]
    fn exists<P: FnOnce(Result<(), glib::Error>) + 'static>(
        &self,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
        callback: P,
    ) {
        let main_context = glib::MainContext::ref_thread_default();
        let is_main_context_owner = main_context.is_owner();
        let has_acquired_main_context = (!is_main_context_owner)
            .then(|| main_context.acquire().ok())
            .flatten();
        assert!(
            is_main_context_owner || has_acquired_main_context.is_some(),
            "Async operations only allowed if the thread is owning the MainContext"
        );

        let user_data: Box_<glib::thread_guard::ThreadGuard<P>> =
            Box_::new(glib::thread_guard::ThreadGuard::new(callback));
        unsafe extern "C" fn exists_trampoline<P: FnOnce(Result<(), glib::Error>) + 'static>(
            _source_object: *mut glib::gobject_ffi::GObject,
            res: *mut gio::ffi::GAsyncResult,
            user_data: glib::ffi::gpointer,
        ) {
            let mut error = std::ptr::null_mut();
            let _ = ffi::polkit_subject_exists_finish(_source_object as *mut _, res, &mut error);
            let result = if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            };
            let callback: Box_<glib::thread_guard::ThreadGuard<P>> =
                Box_::from_raw(user_data as *mut _);
            let callback: P = callback.into_inner();
            callback(result);
        }
        let callback = exists_trampoline::<P>;
        unsafe {
            ffi::polkit_subject_exists(
                self.as_ref().to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                Some(callback),
                Box_::into_raw(user_data) as *mut _,
            );
        }
    }

    fn exists_future(
        &self,
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>> {
        Box_::pin(gio::GioFuture::new(self, move |obj, cancellable, send| {
            obj.exists(Some(cancellable), move |res| {
                send.resolve(res);
            });
        }))
    }

    /// Checks if `self` exists.
    ///
    /// This is a synchronous blocking call - the calling thread is blocked
    /// until a reply is received. See [`exists()`][Self::exists()] for the
    /// asynchronous version.
    /// ## `cancellable`
    /// A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
    ///
    /// # Returns
    ///
    /// [`true`] if the subject exists, [`false`] if not or `error` is set.
    #[doc(alias = "polkit_subject_exists_sync")]
    fn exists_sync(
        &self,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        unsafe {
            let mut error = std::ptr::null_mut();
            let is_ok = ffi::polkit_subject_exists_sync(
                self.as_ref().to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            debug_assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    #[doc(alias = "polkit_subject_hash")]
    fn hash(&self) -> u32 {
        unsafe { ffi::polkit_subject_hash(self.as_ref().to_glib_none().0) }
    }

    /// Serializes `self` to a string that can be used in
    /// [`Subject::from_string()`][crate::Subject::from_string()].
    ///
    /// # Returns
    ///
    /// A string representing `self`. Free with `g_free()`.
    #[doc(alias = "polkit_subject_to_string")]
    fn to_string(&self) -> glib::GString {
        unsafe {
            from_glib_full(ffi::polkit_subject_to_string(
                self.as_ref().to_glib_none().0,
            ))
        }
    }
}

impl<O: IsA<Subject>> SubjectExt for O {}
